## Welcome to Nodysseus!

To get started, checkout:
- the [guides](https://gitlab.com/ulysses.codes/nodysseus/-/tree/main/docs/guides)
- youtube [tutorials](https://www.youtube.com/playlist?list=PLNf6veBQIZNohZk_htvTvPCB2UnEl3Tlh) and [videos](https://www.youtube.com/playlist?list=PLNf6veBQIZNpd8Djjie5W2lo70BkLZotv)
- a reference for all the [standard](https://gitlab.com/ulysses.codes/nodysseus/-/blob/main/docs/reference/nodes.md) and [threejs](https://gitlab.com/ulysses.codes/nodysseus/-/blob/main/docs/reference/three.md) nodes


Handy example files and boilerplates
- [threejs boilerplate](https://nodysseus.io/#threejs_boilerplate): the simplest threejs code without any objects
- [threejs cube](https://nodysseus.io/#threejs_example): a rotating cube
- [threejs force attribute](https://nodysseus.io/#threejs_force_attribute_example): instanced cubes driven by a force geometry attribute
- [hydra](https://nodysseus.io/#hydra_example): hydra synth
